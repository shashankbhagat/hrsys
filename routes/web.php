<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Role;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| HR Web Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware'=>'hr'],  function () {

    Route::get('/hr/dashboard', 'HomeController@hr_dashboard')->name('hr.dashboard');

    Route::resource('/hr/user', 'HRUserController');

});

/*
|--------------------------------------------------------------------------
| SuperVisor Web Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware'=>'supervisor'],  function () {

    Route::get('/supervisor/dashboard', 'HomeController@supervisor_dashboard')->name('supervisor.dashboard');

    Route::get('/supervisor/staff/leave/pending_review', 'SupervisorStaffLeaveController@pending_review')->name('supervisor.leave.pending');

    Route::get('/supervisor/staff/leave/undergoing_review', 'SupervisorStaffLeaveController@undergoing_review')->name('supervisor.leave.undergoing_review');

    Route::get('/supervisor/staff/leave/accepted', 'SupervisorStaffLeaveController@accepted')->name('supervisor.leave.accepted');

    Route::get('/supervisor/staff/leave/rejected', 'SupervisorStaffLeaveController@rejected')->name('supervisor.leave.rejected');

    Route::resource('/supervisor/staff/leave', 'SupervisorStaffLeaveController', ['as' => 'supervisor']);

});



/*
|--------------------------------------------------------------------------
| Staff Web Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware'=>'staff'],  function () {

    Route::get('/staff/dashboard', 'HomeController@staff_dashboard')->name('staff.dashboard');

    Route::resource('/staff/leave', 'StaffLeaveController', ['as' => 'staff']);

});


/*
|--------------------------------------------------------------------------
| Developer Web Routes
|--------------------------------------------------------------------------
|
| These web routes are only for developers. These must be operated
|  only under the supervision of developers. Please comment them after using!
|
*/
 //////////////////////////////////////////////////////////////////////////////////////
 //Create roles when needed, most probably after migrate reset STARTS here
 ////////////////////////////////////////////////////////////////////////////////////

//        Route::get('/createroles', function () {
//
//
//            $role_names = ['HR', 'No role assigned', 'Supervisor', 'Staff'];
//
//            foreach ($role_names as $role_name) {
//
//                $role = new Role;
//
//                $role->name = $role_name;
//
//                $role->save();
//            }
//
//            return "roles created... Please check the data base";
//
//        });

//////////////////////////////////////////////////////////////////////////////////////
//Create roles when needed, most probably after migrate reset ENDS here
////////////////////////////////////////////////////////////////////////////////////
///
///  //////////////////////////////////////////////////////////////////////////////////////
// //Create users when needed, most probably after migrate reset STARTS here
// ////////////////////////////////////////////////////////////////////////////////////
//
//        Route::get('/createusers', function () {
//
//
//            $user_data = [];
//
//            foreach ($role_names as $role_name) {
//
//                $role = new Role;
//
//                $role->name = $role_name;
//
//                $role->save();
//            }
//
//            return "roles created... Please check the data base";
//
//        });
//
////////////////////////////////////////////////////////////////////////////////////////
////Create user when needed, most probably after migrate reset ENDS here
//////////////////////////////////////////////////////////////////////////////////////
