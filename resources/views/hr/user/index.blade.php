@extends('layouts.hr')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @if(Session::has('user_deleted'))
                <div class="alert alert-danger">
                    <strong>{{session('user_deleted')}}!</strong>
                </div>
            @endif

            @if(Session::has('user_created'))
                <div class="alert alert-success">
                    <strong>{{session('user_created')}}!</strong>
                </div>
            @endif

            @if(Session::has('user_updated'))
                <div class="alert alert-info">
                    <strong>{{session('user_updated')}}!</strong>
                </div>
            @endif

            <div class="">
                <div class="card">
                    <div class="card-header">Employee List</div>

                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Role</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if($users)

                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td><img height="50" src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" > </td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->status == 1 ? 'Active' : 'Not Active'}}</td>
                                    <td>{{$user->role->name}}</td>
                                    <td>{{$user->created_at->diffForHumans()}}</td>
                                    <td>{{$user->updated_at->diffForHumans()}}</td>
                                    <td><a href="{{route('user.edit', $user->id)}}"><button class="btn btn-primary">Edit</button></a></td>
                                </tr>

                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
