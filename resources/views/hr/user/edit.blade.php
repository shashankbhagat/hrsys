@extends('layouts.hr')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Employee</div>
                    <div class="card-body">

                        <div class="">
                            <img height="100px" src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" alt="" class="img-responsive img-rounded">
                        </div>

                        <div class="">

                            {!! Form::model($user, ['method' => 'PATCH', 'action'=>['HRUserController@update', $user->id], 'files'=>true]) !!}

                            <label for="name">Name:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {!! Form::text('name', null, ['class'=>'form-control', 'id' => 'name', 'placeholder' => 'Name']) !!}
                            </div>
                            <br>

                            <label for="email">Email:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {!! Form::email('email', null, ['class'=>'form-control', 'id' => 'email', 'placeholder' => 'Email']) !!}
                            </div>
                            <br>

                            <label for="password">Password: (Leave blank for not changing password)</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {!! Form::password('password', ['class'=>'form-control', 'id' => 'password', 'placeholder' => 'Password']) !!}
                            </div>
                            <br>

                            @if(($user->role->name == 'HR') )

                                <label for="role_id">Role:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    {!! Form::select('role_id', array('1'=>'HR'), 'HR', ['class'=>'form-control', 'id' => 'role_id',]) !!}
                                </div>
                                <br>

                                <label for="status">Status:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    {!! Form::select('status', array(1 => 'Active'), null, ['class'=>'form-control', 'id' => 'status']) !!}
                                </div>
                                <br>

                            @else

                                <label for="role_id">Role:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    {!! Form::select('role_id', array(''=>'Select Role') + $roles, $user->role_id, ['class'=>'form-control', 'id' => 'role_id']) !!}
                                </div>
                                <br>

                                <label for="status">Status:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    {!! Form::select('status', array(''=>'Select status', 1 => 'Active', 0 => 'Not Active'), null, ['class'=>'form-control', 'id' => 'status']) !!}
                                </div>
                                <br>

                            @endif

                            <label for="photo_id">Photo:</label>
                            <div class="input-group">
                                {!! Form::file('photo_id', null, ['class'=>'form-control']) !!}
                            </div>
                            <br>

                            <div class="col-sm-3">
                                <div class="input-group">
                                    {!! Form::submit('Update User', ['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                            <br>

                        </div>

                        <div class="">

                            @include('includes.form_error')

                        </div>
                    </div>
                </div><!-- card body close -->
                <br>

                    @if((Auth::user()->email != $user->email) && (Auth::user()->role_id < $user->role_id) )
                    <div class="card">
                        <div class="card-header">

                            {{Auth::user()->role_id}} {{ $user->role_id }}

                            {!! Form::open(['method' => 'DELETE', 'action'=>['HRUserController@destroy', $user->id]]) !!}

                            <div class="input-group">
                                {!! Form::submit('Delete User', ['class'=>'btn btn-danger']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection