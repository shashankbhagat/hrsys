@extends('layouts.hr')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Employee</div>

                    <div class="card-body">

                        {!! Form::open(['method' => 'POST', 'action'=>'HRUserController@store', 'files'=>true]) !!}

                        <label for="name">Name:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::text('name', null, ['class'=>'form-control', 'id' => 'name', 'placeholder' => 'Name']) !!}
                        </div>
                        <br>

                        <label for="email">Email:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::email('email', null, ['class'=>'form-control', 'id' => 'email', 'placeholder' => 'Email']) !!}
                        </div>
                        <br>

                        <label for="password">Password:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::password('password', ['class'=>'form-control', 'id' => 'password', 'placeholder' => 'Password']) !!}
                        </div>
                        <br>

                        <label for="role_id">Role:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::select('role_id', array('' => 'Select a role') + $roles, 'null', ['class'=>'form-control', 'id' => 'role_id']) !!}
                        </div>
                        <br>

                        <label for="status">Status:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::select('status', array(''=>'Select status', 1 => 'Active', 0 => 'Not Active'), null, ['class'=>'form-control', 'id' => 'status']) !!}
                        </div>
                        <br>

                        <label for="photo_id">Photo:</label>
                        <div class="input-group">
                            {!! Form::file('photo_id', null, ['class'=>'form-control']) !!}
                        </div>
                        <br>

                        <div class="input-group">
                            {!! Form::submit('Create User', ['class'=>'btn btn-primary']) !!}
                        </div>

                        {!! Form::close() !!}



                    </div>
                </div>
                <br>

                @include('includes.form_error')
                </div>
            </div>
        </div>
    </div>
@endsection
