@extends('layouts.supervisor')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if(Session::has('leave_deleted'))
                <div class="alert alert-danger">
                    <strong>{{session('leave_deleted')}}!</strong>
                </div>
            @endif

            @if(Session::has('leave_created'))
                <div class="alert alert-success">
                    <strong>{{session('leave_created')}}!</strong>
                </div>
            @endif

            @if(Session::has('leave_updated'))
                <div class="alert alert-info">
                    <strong>{{session('leave_updated')}}!</strong>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col">
                <div class="card">

                    <div class="card-header">Leave List</div>
                    <div class="card-body">

                        <div class="alert alert-info" role="alert">
                            Once the status has been set to the following 'Accepted or Rejected', further status changes cannot be made by the supervisor.
                            <br> When the status is 'Pending review, the staff can make edit the application details from their portal'.
                            <br> To prevent staff from further editing the application details set the status to 'Undergoing Review'
                        </div>
                        <table class="table table-responsive ">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Status</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Applicant's Name</th>
                                <th>Photo</th>
                                <th>Edit</th>
                                <th>Reason</th>
                                <th>Created at</th>
                                <th>Updated at</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if($leaves)

                                @foreach($leaves as $leave)

                                    <tr>
                                        <td>{{$leave->id}}</td>
                                        <td>
                                            @if($leave->status == 0)
                                                <div class="alert-danger"> Rejected </div>
                                            @elseif($leave->status == 1)
                                                <div class="alert-success"> Accepted </div>
                                            @elseif($leave->status == 2)
                                                <div class="alert-info"> Undergoing Review </div>
                                            @elseif($leave->status == 3)
                                                <div class="alert-warning"> Pending Review </div>
                                            @endif
                                        </td>
                                        <td>{{$leave->start_date}}</td>
                                        <td>{{$leave->end_date}}</td>
                                        <td>{{$leave->user->name}}</td>
                                        <td>
                                            <img height="50" src="{{$leave->photo ? $leave->photo->file : 'http://placehold.it/400x400'}}">
                                        </td>
                                        <td>
                                            @if($leave->status == 0 || $leave->status == 1)
                                                Edit Not Available
                                            @else
                                                <a href="{{route('supervisor.leave.edit', $leave->id)}}"><button class="btn btn-primary">Change Status</button></a>
                                            @endif
                                        </td>

                                        <td>{{$leave->reason}}</td>
                                        <td>{{$leave->created_at->diffForHumans()}}</td>
                                        <td>{{$leave->updated_at->diffForHumans()}}</td>

                                    </tr>

                                @endforeach

                            @endif


                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
