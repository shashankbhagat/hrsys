@extends('layouts.supervisor')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">


                        {!! Form::model($leave, ['method' => 'PATCH', 'action'=>['SupervisorStaffLeaveController@update', $leave->id], 'files'=>true]) !!}

                        <label for="start_date">Start Date:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::date('start_date', null, ['class'=>'form-control', 'id' => 'start_date', 'disabled'=>'true']) !!}
                        </div>
                        <br>

                        <label for="end_date">End Date:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::date('end_date', null, ['class'=>'form-control', 'id' => 'end_date', 'disabled'=>'true']) !!}
                        </div>
                        <br>

                        <label for="photo_id">Photo:</label>
                        <div class="input-group">
                            <img height="100px" src="{{$leave->photo ? $leave->photo->file : 'http://placehold.it/400x400'}}" alt="" class="img-responsive img-rounded">
                        </div>
                        <br>

                        <label for="reason">Reason:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::textarea('reason', null, ['class'=>'form-control', 'id' => 'reason', 'disabled'=>'true']) !!}
                        </div>
                        <br>

                        <label for="status">Change Status:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::select('status', array(''=>'Select Status','0'=>'Rejected', '1'=>'Accepted', '2'=>'Undergoing Review', '3'=>'Pending Review'), $leave->status, ['class'=>'form-control', 'id' => 'status']) !!}
                        </div>
                        <br>

                        <div class="input-group">
                            {!! Form::submit('Apply', ['class'=>'btn btn-primary']) !!}
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
