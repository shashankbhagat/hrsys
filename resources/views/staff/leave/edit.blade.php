@extends('layouts.staff')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Leave</div>

                    <div class="card-body">

                        {!! Form::model($leave, ['method' => 'PATCH', 'action'=>['StaffLeaveController@update', $leave->id], 'files'=>true]) !!}

                        <label for="start_date">Start Date:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::date('start_date', null, ['class'=>'form-control', 'id' => 'start_date']) !!}
                        </div>
                        <br>

                        <label for="end_date">End Date:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::date('end_date', null, ['class'=>'form-control', 'id' => 'end_date']) !!}
                        </div>
                        <br>

                        <label for="photo_id">Photo:</label>
                        <div class="input-group">
                            {!! Form::file('photo_id', null, ['class'=>'form-control']) !!}
                        </div>
                        <br>

                        <label for="reason">Reason:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::textarea('reason', null, ['class'=>'form-control', 'id' => 'reason']) !!}
                        </div>
                        <br>

                        <div class="input-group">
                            {!! Form::submit('Apply', ['class'=>'btn btn-primary']) !!}
                        </div>

                        {!! Form::close() !!}


                    </div>

                    @include('includes.form_error')

                </div>
            </div>
        </div>
    </div>
@endsection
