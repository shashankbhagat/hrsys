-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 18, 2019 at 11:55 AM
-- Server version: 5.6.40
-- PHP Version: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_interview`
--

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '3',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leaves`
--

INSERT INTO `leaves` (`id`, `user_id`, `status`, `start_date`, `end_date`, `reason`, `photo_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, '2019-01-01', '2019-01-03', 'Brother\'s Marriage . Card photo also attached', 2, '2019-01-10 03:33:47', '2019-01-10 04:12:04'),
(2, 2, 1, '2019-01-05', '2019-01-05', 'Going to meet doctor for routine checkup', NULL, '2019-01-10 03:34:28', '2019-01-10 04:08:15'),
(3, 2, 3, '2019-01-09', '2019-01-12', 'Going to pune', NULL, '2019-01-10 03:35:54', '2019-01-10 03:35:54'),
(4, 2, 0, '2019-01-22', '2019-01-29', 'Going to goa', NULL, '2019-01-10 03:36:34', '2019-01-10 04:11:47'),
(5, 3, 3, '2019-01-15', '2019-01-16', 'Home Redecoration has to be done', NULL, '2019-01-10 03:43:15', '2019-01-10 03:43:27'),
(6, 2, 3, '2019-01-24', '2019-01-17', 'reason', NULL, '2019-01-18 00:10:38', '2019-01-18 00:10:38'),
(7, 5, 3, '2019-01-19', '2019-01-05', 'DSFSD', NULL, '2019-01-18 00:11:36', '2019-01-18 00:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_09_104649_create_roles_table', 1),
(4, '2019_01_09_145344_create_photos_table', 1),
(5, '2019_01_09_171818_create_leaves_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `file`, `created_at`, `updated_at`) VALUES
(1, '1547110853IMG_20180828_153050.jpg', '2019-01-10 03:30:53', '2019-01-10 03:30:53'),
(2, '1547111105b0b57bb262a001a7b46dd400d8a509b0.jpg', '2019-01-10 03:35:05', '2019-01-10 03:35:05'),
(3, '1547111524WhatsApp Image 2018-11-28 at 8.58.26 PM.jpeg', '2019-01-10 03:42:04', '2019-01-10 03:42:04');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'HR', '2019-01-09 09:37:09', '2019-01-09 09:37:09'),
(2, 'No Roles Assigned', '2019-01-09 09:37:09', '2019-01-09 09:37:09'),
(3, 'Supervisor', '2019-01-09 09:37:09', '2019-01-09 09:37:09'),
(4, 'Staff', '2019-01-09 18:30:00', '2019-01-09 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `status` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `status`, `name`, `email`, `email_verified_at`, `password`, `photo_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Pooja', 'hr@gmail.com', NULL, '$2y$10$EsUssG112pOTLhshMyeDMuv4xjZbOWu67lzf5Xqwz1GrYM1IXdRxG', NULL, 'FCB7wHQFWXnMWpjArLQQItCFGjYltw2j2JSx3AJ1tWt2Eh0qhmSrz8Kuw1gU', '2019-01-10 03:21:55', '2019-01-10 03:21:55'),
(2, 4, 1, 'Shashank Bhagat', 'staff1@gmail.com', NULL, '$2y$10$1mFhsDTPgsUr8e8x.9Sk0.i8T9lVuKcqN3Hv5z3GOqQ5VnfWg4J0m', '1', 'NA40TwOcHgPx30cc4ytFabofh3ztRzIaIGQh62xjjDxjAfuFxgPfGRLUiQba', '2019-01-10 03:30:53', '2019-01-10 03:30:53'),
(3, 4, 1, 'Rohit Kumar', 'staff2@gmail.com', NULL, '$2y$10$KzSnjq89bJIGdZd0RvlmJeZZnH/uAumct/R3.6v.UAGNfg3rkvHqW', '3', 'hf1r4WLfolMamEEXkjNeEzpPJclKibGtLddeJOnOq4hMaz6YHvOUm7voWBmg', '2019-01-10 03:42:04', '2019-01-10 03:42:04'),
(4, 3, 1, 'Nithin', 'supervisor@gmail.com', NULL, '$2y$10$DQ.rTPlfOi92eX07S1BFRuQqAismKgMMF2yo7EtE1wqwHaKzYMBMy', NULL, 'ukrRpz6xPlxLfX3mdQlsdl61MJ7pQ2tF2tpxMmrvXTWw4w33672Z0F5ZgooK', '2019-01-10 04:06:44', '2019-01-10 04:07:19'),
(5, 4, 1, 'dinesh naik', 'dineshnaik.svit@gmail.com', NULL, '$2y$10$BJ4C8H/4emz.pgKaKH9wpu99MECX.pfofYdXquhlHQQg93s8JOAI2', NULL, NULL, '2019-01-18 00:10:17', '2019-01-18 00:10:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leaves_user_id_index` (`user_id`),
  ADD KEY `leaves_photo_id_index` (`photo_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
