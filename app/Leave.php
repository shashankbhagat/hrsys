<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    //
    protected $fillable = [

        'user_id', 'status', 'start_date', 'end_date', 'reason', 'photo_id',

    ];

    public function user() {

        return $this->belongsTo('App\User');

    }

    public function photo() {

        return $this->belongsTo('App\Photo');
    }

}
