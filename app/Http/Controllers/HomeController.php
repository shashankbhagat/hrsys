<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    /*
     * Above this is default code
     * Below this are custom functions
     */
    public function hr_dashboard() {

        return view('hr_dashboard');
    }

    public function supervisor_dashboard() {

        return view('supervisor_dashboard');
    }

    public function staff_dashboard() {

        return view('staff_dashboard');
    }
}
