<?php

namespace App\Http\Controllers;

use App\Leave;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SupervisorStaffLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $leaves = Leave::all();

        return view('supervisor.leave.index', compact('leaves'));
    }
    public function pending_review()
    {
        $leaves = Leave::all()->where('status', '==', '3');

        return view('supervisor.leave.index', compact('leaves'));
    }

    public function undergoing_review()
    {
        $leaves = Leave::all()->where('status', '==', '2');

        return view('supervisor.leave.index', compact('leaves'));
    }

    public function accepted()
    {
        $leaves = Leave::all()->where('status', '==', '1');

        return view('supervisor.leave.index', compact('leaves'));
    }

    public function rejected()
    {
        $leaves = Leave::all()->where('status', '==', '0');

        return view('supervisor.leave.index', compact('leaves'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $leave = Leave::findorfail($id);

        return view('supervisor.leave.edit', compact('leave'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $leave = Leave::findOrFail($id);

        $input = $request->all();

        $leave->update($input);

        Session::flash('leave_updated', 'Leave details has been updated');

        return redirect(route('supervisor.leave.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
